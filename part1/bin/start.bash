docker run --rm --detach --name 'web1' -p 10000:80 --mount type="bind",src="$(pwd)/web1",dst="/usr/share/nginx/html" nginx:1.19.2
docker run --rm --detach --name 'web2' -p 10001:80 --mount type="bind",src="$(pwd)/web2",dst="/usr/share/nginx/html" nginx:1.19.2
docker run --rm --detach --name 'db1' -p 20000:80 --mount type=bind,src="$(pwd)/db1",dst="/data/db" mongo:4.4.1
docker run --rm --detach --name 'db2' -p 20001:80 --mount type=bind,src="$(pwd)/db2",dst="/data/db" mongo:4.4.1
