# Homework 1 - Docker

## Setup

1. Fork this project into your private individual group on GitLab for this course.
2. In your fork, remove its fork relationship to this project. Do this by opening
   your project's general settings, and look in the Advanced section.
3. Clone ***your fork*** (not this project) to your local machine.

## Part 1 - Managing a Docker system using Bash

For this part, you will be working with just the files and directories in
the `part1/` subdirectory. This directory is the "root of the project" for
the purposes of this part. The remainder of this parts instructions will
only refer to subdiretories within `part1/` and all relative paths are
relative to this directory.

`bin/` contains three empty Bash scripts: `bin/start.bash`, `bin/logs.bash`,
and `bin/stop.bash`. In this part, you will implement these three scripts.

Each script int `bin/` is intended to be ran in the root directory of this
project (remember, that's `part1/`). For example,

```
cd /path/to/Homework1/part1
bin/start.bash
bin/logs.bash
bin/status.bash
bin/stop.bash
```

`bin/start.bash` must use Docker commands to start two NGINX web servers and
two Mongo DB servers. The first webserver must be named `web1`, accessible
through <http://localhost:10000/>, and serve the web site in the `web1/`
subdirectory. The second webserver should be named `web2`, accessible through
<http://localhost:10001/> and should serve the web site in the `web2/`
subdirectory. See <https://hub.docker.com/_/nginx> for more information on
the NGINX container. In particular, you need to figure out what directory
it serves static web pages from so that you can mount `web1/` and `web2/`
into them. Also, use a version tag for the image (and not `latest`) to ensure
that I will get the exact same image that you do. See <https://hub.docker.com/_/nginx>
for image tags, and choose the one that maps to the same image as `latest`, but won't
change when a new release is released.

The first Mongo DB server will support the first webserver. Name
it `db1` and save its data to `db1/`. Similarly name the second Mongo DB server
`db2` and save its data to `db2/`. The start script should download the latest
NGINX and Mongo images if they don't exist locally, create the necessary
containers from those images, run the containers, and return. Note, the Mongo
servers are not connected to our webserver at this time, nor are they available
through localhost. We're just getting practice setting up the servers we need.
See <https://hub.docker.com/_/mongo> to learn more about the Mongo image; in
particular, you need to figure out where to mount its data directory. Also,
use a version tag for the image (and not `latest`) to ensure
that I will get the exact same image that you do. See <https://hub.docker.com/_/mongo>
for image tags, and choose the one that maps to the same image as `latest`, but won't
change when a new release is released.

`bin/logs.bash` must print the logs of all the running containers in this system
(and only the containers in this system).

`bin/status.bash` must display all processes running in docker, running or stopped.
It may print the status of containers that are not part of this system.

`bin/stop.bash` must stop and remove all containers in this system.

Once your scripts are working, stop and remove all your project's containers. Then
run the following commands from the root of this project.

```
bin/start.bash
sleep 10
bin/logs.bash > logs
bin/status.bash > status
bin/stop.bash
git add .
git commit -m "part1: Add logs and status of test run"
git push
```

## Part 2 - Managing a Docker system using Docker Compose

For this part, you will be working with just the files and directories in
the `part2/` subdirectory. This directory is the "root of the project" for
the purposes of this part. The remainder of this parts instructions will
only refer to subdiretories within `part2/` and all relative paths are
relative to this directory.

The root of this project contains an empty `docker-compose.yml` file.
In this file, specify the same system as in Part 1. Use
<https://docs.docker.com/compose/compose-file/>
as a reference. Once implemented,
the commands above are roughly equivalent to the following commands.

```
cd /path/to/Homework1/part2
docker-compose up --detach
docker-compose logs
docker-compose ps
docker-compose down
```

Note that `docker-compose ps` only shows containers of services declared in
docker-compose.yml. That may be different from your scripts above, and that's fine.

Once your YAML file is working, stop and remove all your project's containers. Then
run the following commands from the root of this project.

```
docker-compose up --detach
sleep 10
docker-compose logs > logs
docker-compose ps > status
docker-compose down
git add .
git commit -m "part2: Add logs and status of test run"
git push
```

## Submission

The git commands at the end of each part, if they complete without error,
will have submitted your work to your repository. That's all you need to do.
If you make a change after running those commands, just rerun all of the
commands in the section for the part that you changed.